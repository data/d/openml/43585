# OpenML dataset: My-Clash-Royale-Ladder-Battles

https://www.openml.org/d/43585

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Clash Royale Ladder Battles dataset
I expect to update this dataset daily
I encourage you to make an EDA or even train a model to predict if I win or lose!
Notes:

"op" means opponent
my/optroops, my/opbuildings, my/opspells, my/opcommons, my/oprares, my/opepics, my/op_legendaries: Number of cards of that type. 
my/opnameof_card: Level of that card. If 0 then that card wasn't used in that battle.

This is my second Clash Royale account and I don't play 2v2 matches, 1v1 ladder matches only.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43585) of an [OpenML dataset](https://www.openml.org/d/43585). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43585/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43585/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43585/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

